#define _CRT_SECURE_NO_WARNING
#include "config\help.h"
#include"frontend/Ch.h"
#include"frontend/Emoji.h"
#include"frontend/login/F_Login.h"
#include"frontend/login/interface/F_L_Login.h"
#include"frontend/login/interface/F_L_Start.h"
#include"frontend/login/interface/F_L_Regist.h"
#include"frontend/center/F_Center.h"
#include"backend/center/B_Center.h"
#include"utils/db-lite/LiteConn.h"
#include"backend/center/GetFilePath.h"
#include <windows.h>
#include <iostream>

int main() {
	//std::cout << "hello world" << std::endl;

	//删除AccountTable
	//LiteConn db("EmojiSystem");
	//std::string deleteTableSQL = R"(
	//	drop table AccountTable
 //   )";
	//db.update(deleteTableSQL);

	


	B_Login b_login;
	b_login.initAccountTable();
	B_Center b_center;
	b_center.initCenterTable();
	

	//---创建窗口---	
	initgraph(1120, 700);
	setbkcolor(WHITE);
	cleardevice();

	Front* Interface = new F_L_Start;

	BeginBatchDraw();
	while (1)
	{
		ExMessage msg;

		if (peekmessage(&msg))
		{
			//1、绘制界面
			Interface->Draw();

			//2、更新界面
			Interface->Update(msg);

			//3、退出界面
			ButtonDistinction distinction = Interface->Quit(msg);
			if (distinction == QuitSystem)
			{
				delete Interface;
				EndBatchDraw();
				closegraph();
				return 0;
			}
			else if (distinction == Start)
			{
				delete Interface;
				Interface = new F_L_Start;
			}
			else if (distinction == Login)
			{
				delete Interface;
				Interface = new F_L_Login;
			}
			else if (distinction == Regist)
			{
				delete Interface;
				Interface = new F_L_Regist;
			}
			else if (distinction == Center)
			{
				string tempAccount = Interface->Account;
				delete Interface;
				Interface = new F_Center(tempAccount);
				/*Interface = new F_Center;
				F_Center->Account = account1;*/
			}



			FlushBatchDraw();
		}
	}
	EndBatchDraw();

	delete Interface;

	system("pause");

	return 0;
}