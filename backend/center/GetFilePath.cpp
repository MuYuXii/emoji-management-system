#include "GetFilePath.h"

std::string GetFilePath::selectFile()
{
	OPENFILENAME ofn;
	TCHAR szOpenFileNames[80 * MAX_PATH] = { 0 };
	TCHAR szPath[MAX_PATH];
	TCHAR szFileName[80 * MAX_PATH];

	int nLen = 0;
	TCHAR* p = NULL;
	ZeroMemory(&ofn, sizeof(ofn));

	// 结构体大小
	ofn.lStructSize = sizeof(ofn);
	// 拥有着窗口句柄
	ofn.hwndOwner = NULL;
	// 接收返回的文件名，注意第一个字符需要为NULL
	ofn.lpstrFile = szOpenFileNames;
	// 缓冲区长度
	ofn.nMaxFile = sizeof(szOpenFileNames);
	// _T可替换为TEXT，使用_T需要引tchar.h
	ofn.lpstrFile[0] = '\0';
	// 设置过滤[{显示名名称}\0{正则形式的文件类型}\0\0]
	ofn.lpstrFilter = "All\0*.*\0.h\0*.h\0.hpp\0*.hpp\0.c\0*.c\0.cpp\0*.cpp\0.cc\0*.cc\0.cxx\0*.cxx\0\0";
	// 过滤器索引
	ofn.nFilterIndex = 1;
	// 窗口标题
	ofn.lpstrTitle = "请选择文件";
	// 文件必须存在、允许多选、隐藏只读选项、对话框使用资源管理器风格的用户界面
	// 官方文档：https://docs.microsoft.com/en-us/windows/win32/api/commdlg/ns-commdlg-openfilenamea
	ofn.Flags = OFN_FILEMUSTEXIST | OFN_ALLOWMULTISELECT | OFN_HIDEREADONLY | OFN_EXPLORER;
	// 如果打开文件失败，则不操作

	char path[100] = { 0 };
	DWORD pathLength = GetCurrentDirectoryA(MAX_PATH, path);

	if (!::GetOpenFileName(&ofn)) {
		return new char[0];
	}

	SetCurrentDirectoryA(path);

	// 把第一个文件名前的复制到szPath,即:  
	// 如果只选了一个文件,就复制到最后一个'/'  
	// 如果选了多个文件,就复制到第一个NULL字符  
	lstrcpyn(szPath, szOpenFileNames, ofn.nFileOffset);
	// 当只选了一个文件时,下面这个NULL字符是必需的.  
	// 这里不区别对待选了一个和多个文件的情况
	szPath[ofn.nFileOffset] = '\0';
	nLen = lstrlen(szPath);
	// 如果选了多个文件,则必须加上'//'  
	if (szPath[nLen - 1] != '\\') {
		lstrcat(szPath, "\\");
	}

	// 把指针移到第一个文件  
	p = szOpenFileNames + ofn.nFileOffset;
	// 对szFileName进行清零
	ZeroMemory(szFileName, sizeof(szFileName));
	// 定义字符串，用于拼接所选的所有文件的完整路径
	std::string str = "";

	while (*p) {
		// 读取文件名
		std::string fileName = (p);
		// 读取文件所在文件夹路径
		std::string filePath = (szPath);
		// 拼接文件完整路径
		std::string completePath = filePath + fileName;
		// 拼接字符串
		str += completePath;
		//移至下一个文件
		p += lstrlen(p) + 1;
	}

	return str;
}
