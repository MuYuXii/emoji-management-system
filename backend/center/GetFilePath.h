#pragma once

#include <windows.h>
#include <shlobj.h>
#include "../../config/help.h"

class GetFilePath
{
public:
	GetFilePath() = default;
	~GetFilePath() = default;
	
	std::string selectFile();

};

