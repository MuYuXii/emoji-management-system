#pragma once
#include "../../config/help.h"
#include"../../utils/db-lite/LiteConn.h"
#include <vector>
#include <string>
#include <algorithm>
class B_Center{
public:
    B_Center() = default;
    ~B_Center() = default;

    bool initCenterTable();
    //// 添加表情包
    //void addEmoji(std::string path);
    
    //添加表情包到账号
    void addEmoji(std::string path, std::string accountId, std::string name);
    //获取路径中的文件名
    std::string getImgName(std::string path);
    // 删除表情包
    void deleteEmoji(std::string emoji_name, std::string addount_id);
    int searchBigEmojiName();
    // 搜索表情包   
    char** searchEmojiByID(std::string accountId, int& nrows, int& ncols);
    char** searchEmojiByEmjName(std::string accountId, int& nrows, int &ncols);
    // 更改表情包信息
    void updateEmoji(std::string emjName, std::string accountId, int isLike);
};