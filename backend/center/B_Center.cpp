#include "B_Center.h"
using namespace std;


/*
    下面是SQLite3的使用示例
    已经封装了SQLite3在 utils/db-lite 文件夹下的 LiteConn.h

    //步骤1:初始化数据库(如果数据库不存在，请填写数据库名字段）
    LiteConn db(students.db);

    //步骤2:创建表
    std::string createTableSQL = R"(
        CREATE TABLE IF NOT EXISTS Students(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            age INTEGER,
            gender TEXT
        )
    )";
    dp.update(creatTableSQL);

    //步骤3:(这一步取决于你) insert、require、update、delete
        // insert into students
    std::string insertSQL = R"(
        INSERT INTO 
        Students (name, age, gender) 
        VALUES ('John Doe', 20, 'Male')
    )";
    bool result = db.update(insertSQL);
    if (result) {
        LOG("Insert successful.")
    } else {
        LOG("Insert failed.")
    }


        // require from students
    std::string selectSQL = R"(SELECT * FROM Students)";
    int nrows, ncols;
    char **data = db.query(selectSQL, nrows, ncols);
    if (data) {
        for (int i = 0; i < nrows; i++) { 
            for (int j = 0; j < ncols; j++) { 
                // data[i * ncols + j] 
            }
        }
        sqlite3_free_table(data);
    } else {
        LOG("Query failed.")
    }
        // update and delete as same as insert, you can simulate it and focus on SQL that is diff
        //更新和删除与插入一样，您可以模拟它并专注于不同的SQL







    // 下面是创建文件夹和从 path 路径复制到创建的这个文件夹下的示例
        // 创建目录
        bool createDirectory(const std::string& dirPath) {
            int result = mkdir(dirPath.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);
            return result == 0;
        }

        // 获取目录下所有文件的路径
        std::vector<std::string> getFilesInDirectory(const std::string& dirPath) {
            std::vector<std::string> files;
            DIR* dir = opendir(dirPath.c_str());
            struct dirent* entry;
            if (dir) {
                while ((entry = readdir(dir)) != NULL) {
                    if (entry->d_type == DT_REG) { // 检查是否为普通文件
                        files.push_back(std::string(entry->d_name));
                    }
                }
                closedir(dir);
            }
            return files;
        }

        // 移动并重命名文件
        bool moveAndRenameFile(const std::string& sourceFilePath, const std::string& targetDir, const std::string& newName) {
            std::string targetFilePath = targetDir + "/" + newName;
            return rename(sourceFilePath.c_str(), targetFilePath.c_str()) == 0;
        }

        int main() {
            // 指定源路径和目标文件夹名称
            std::string sourcePath = "/path/to/source"; // 替换为实际的源路径
            std::string targetFolder = "/path/to/target_folder"; // 替换为实际的目标文件夹路径
            std::string name = "name"; // 将要重命名的文件名

            // 创建目标文件夹
            if (!createDirectory(targetFolder)) {
                std::cerr << "Failed to create directory: " << targetFolder << std::endl;
                return 1;
            }

            // 获取源路径下的所有文件
            std::vector<std::string> files = getFilesInDirectory(sourcePath);
            for (const auto& file : files) {
                // 构建完整的源文件路径
                std::string sourceFilePath = sourcePath + "/" + file;
                // 构建目标文件的完整路径
                std::string targetFilePath = targetFolder + "/" + name + std::string(getFileExtension(file));

                // 移动文件到目标文件夹并重命名
                if (moveAndRenameFile(sourceFilePath, targetFolder, name)) {
                    std::cout << "Moved and renamed: " << sourceFilePath << " -> " << targetFilePath << std::endl;
                } else {
                    std::cerr << "Failed to move and rename file: " << sourceFilePath << std::endl;
                }
            }

            return 0;
        }

        // 获取文件扩展名
        std::string getFileExtension(const std::string& filename) {
            size_t dotPos = filename.find_last_of(".");
            if (dotPos != std::string::npos) {
                return filename.substr(dotPos);
            }
            return "";
        }
*/



bool B_Center::initCenterTable()
{
    LiteConn db("EmojiSystem");
    //创建CenterTable表
    std::string createTableSQL = R"(
        CREATE TABLE IF NOT EXISTS EmojiTable(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
			emoji_name TEXT NOT NULL,
			account_id TEXT NOT NULL,
            is_like INTEGER NOT NULL,
            emoji_path TEXT NOT NULL
        )
    )";

    db.update(createTableSQL);
    //给visitor账号初始化 1~50号表情包
    /*for (int i = 1; i <= 50; i++)
    {
        string insertSQL = R"(
        INSERT OR IGNORE INTO
        AccountTable (emoji_name,account_id, is_like,emoji_path)
        VALUES ('%s','visitor',0 , './././picture/%s')
	)";
        char temp[1000];
        string emjName = to_string(i);
        string emjPath = to_string(i) + ".jpg";
        strcpy(temp, insertSQL.data());
        sprintf(temp, insertSQL.data(), emjName.data(),emjPath.data());
        insertSQL = temp;
        bool result = db.update(insertSQL);
        if (result) {
            LOG("%s insert successful.")
        }
        else {
            LOG("%s has been insert")
        }
    }*/
    

    //查询表中所有表情包

    string selectSQL = R"(SELECT * FROM EmojiTable)";
    int nrows, ncols;
    char** data = db.query(selectSQL, nrows, ncols);
    if (data) {
        for (int i = 0; i < nrows + 1; i++) {
            for (int j = 0; j < ncols; j++) {
                cout << data[i * ncols + j] << "  ";
            }
            cout << endl;
        }
        sqlite3_free_table(data);
    }
    else {
        LOG("Query failed.")
    }
    return true;
}

/*
    将path路径添加到表中，并创建一个文件夹并将路径的表情包复制到这个新的文件夹下
    （表设计时将设置id为自动递增）
*/
void B_Center::addEmoji(string path, string accountId, string name){
    
    LiteConn db("EmojiSystem");

    string insertSQL = R"(
        INSERT OR IGNORE INTO 
        EmojiTable (emoji_name, account_id, is_like, emoji_path)
        VALUES ('%s', '%s', 0 , '%s')
	)";
    char temp[1000];
    strcpy(temp, insertSQL.data());
    sprintf(temp, insertSQL.data(), name.data(), accountId.data(), path.data());
    insertSQL = temp;
    bool result = db.update(insertSQL);
    if (result) {
        LOG("%s insert successful.")
    }
    else {
        LOG("%s has been insert")
    }
}

//void B_Center::addEmoji(string imgName, string addCountId){
//    LiteConn db("EmojiSystem");
//    string imgPath = imgName + ".png";
//    string insertSQL = R"(
//        INSERT OR IGNORE INTO 
//        AccountTable (emoji_name,account_id, is_like,emoji_path)
//        VALUES ('%s','%s',0 , './././picture/%s')
//	)";
//    char temp[1000];
//    strcpy(temp, insertSQL.data());
//    sprintf(temp, insertSQL.data(), imgName.data(),addCountId.data(), imgPath.data());
//    insertSQL = temp;
//    bool result = db.update(insertSQL);
//    if (result) {
//        LOG("%s insert successful.")
//    }
//    else {
//        LOG("%s has been insert")
//    }
//}

string B_Center::getImgName(string path){
    string str = path;
    //截取‘/’后面的文件名
    size_t pos = str.find_last_of('/');
    string imgName = str.substr(pos + 1);
    //截取‘.’前面的文件名
    size_t dotPos = imgName.find_last_of('.');
    imgName = imgName.substr(0, dotPos);
    return imgName;
}


/*  
    根据选中的表情包的id去删除表中数据，或者修改形参实现批量删除等
*/
void B_Center::deleteEmoji(string emjName,string accountId){
    LiteConn db("EmojiSystem");
    string imgPath = emjName + ".jpg";
    string deleteSQL = R"(
        DELETE FROM EmojiTable
        WHERE emoji_name = '%s' AND account_id = '%s'
    )";
    char temp[1000];
    strcpy(temp, deleteSQL.data());
    sprintf(temp, deleteSQL.data(), emjName.data(), accountId.data(), imgPath.data());
    deleteSQL = temp;
    bool result = db.update(deleteSQL);
    if (result) {
        LOG("%s delete successful.")
    }else {
        LOG("%s has not exist")
    }
}

int B_Center::searchBigEmojiName() {
    LiteConn db("EmojiSystem");
    string selectSQL = R"(SELECT MAX (CAST(emoji_name AS INEGER))
                          FROM EmojiTable 
                        )";
    int nrows, ncols;
    char** data = db.query(selectSQL, nrows, ncols);
    if (data) {
        int maxEmojiName = 0;
        if (nrows > 0 && ncols > 0) {
            if (data[1] == NULL)
                maxEmojiName = 0;
            else
                maxEmojiName = atoi(data[1]);
        }
        sqlite3_free_table(data);
        return maxEmojiName;
    }
    else {
        LOG("Query failed.")
            return -1;
    }

}

/*
    这个函数需要将返回值修改为一个结构体类型，其中存放的为一个结构体对应表的字段结构（详细问一下）
    这个函数直接被F_Center.cpp调用，其在初始化时会直接调用searchEmoji来查询表中所有的路径并进行渲染；在添加后会再次调用刷新渲染
*/
char** B_Center::searchEmojiByID(string accountId, int& nrows, int& ncols){
    LiteConn db("EmojiSystem");
    string selectSQL = R"(SELECT emoji_name, emoji_path, is_like FROM EmojiTable
                          WHERE account_id='%s' 
                        )";
    char temp[1000];
    strcpy(temp, selectSQL.data());
    sprintf(temp, selectSQL.data(), accountId.data());
    selectSQL = temp;
    char** data = db.query(selectSQL, nrows, ncols);
    if (data) {
        for (int i = 0; i < nrows + 1; i++) {
            for (int j = 0; j < ncols; j++) {
                cout << data[i * ncols + j] << "  ";
            }
            cout << endl;
        }
        //sqlite3_free_table(data);
    }
    else {
        LOG("Query failed.")
    }
    return data;
}

//char** B_Center::searchEmojiByEmjName(string EmojiName,string accountId)
//{
//    LiteConn db("EmojiSystem");
//    string selectSQL = R"(SELECT * FROM EmojiTable
//                          WHERE emoji_name='%s' AND account_id='%s'
//                        )";
//    char temp[1000];
//    strcpy(temp, selectSQL.data());
//    sprintf(temp, selectSQL.data(), EmojiName.data(), accountId.data());
//    selectSQL = temp;
//    int nrows, ncols;
//    char** data = db.query(selectSQL, nrows, ncols);
//    if (data) {
//        for (int i = 0; i < nrows + 1; i++) {
//            for (int j = 0; j < ncols; j++) {
//                cout << data[i * ncols + j] << "  ";
//            }
//            cout << endl;
//        }
//        sqlite3_free_table(data);
//    }
//    else {
//        LOG("Query failed.")
//    }
//    return data;


char** B_Center::searchEmojiByEmjName(string accountId, int& nrows, int& ncols)
{
    LiteConn db("EmojiSystem");
    string selectSQL = R"(SELECT emoji_name, emoji_path, is_like FROM EmojiTable
                          WHERE account_id='%s'
                        )";
    char temp[1000];
    strcpy(temp, selectSQL.data());
    sprintf(temp, selectSQL.data(),accountId.data());
    selectSQL = temp;
    char** data = db.query(selectSQL, nrows, ncols);
    if (data) {
        for (int i = 0; i < nrows + 1; i++) {
            for (int j = 0; j < ncols; j++) {
                cout << data[i * ncols + j] << "  ";
            }
            cout << endl;
        }
        sqlite3_free_table(data);
    }
    else {
        LOG("Query failed.")
    }
    return data;
}

/*
    更新表中的字段值
*/
void B_Center::updateEmoji(string emjName, string accountId, int isLike){
    LiteConn db("EmojiSystem");

    string updateSQL = R"(
        UPDATE EmojiTable
        SET is_like = %d
        WHERE emoji_name = '%s' AND account_id = '%s'
    )";
    char temp[1000];
    strcpy(temp, updateSQL.data());
    sprintf(temp, updateSQL.data(), isLike, emjName.data(), accountId.data());
    updateSQL = temp;
    bool result = db.update(updateSQL);
    if (result) {
        LOG("%s insert successful.")
    }
    else {
        LOG("%s has been insert")
    }
    return;
}
