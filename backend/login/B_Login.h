#pragma once

#include "../../utils/db-lite/LiteConn.h"

class B_Login{
public:
    B_Login() = default;
    ~B_Login() = default;

	bool initAccountTable();

    bool verifyAccount(std::string account, std::string password);
    bool registerAccount(std::string account, std::string password);
};
