#include "B_Login.h"

bool B_Login::initAccountTable()
{
	{
		//初始化
		LiteConn db("EmojiSystem");

		//创建AccountTable表
		std::string createTableSQL = R"(
        CREATE TABLE IF NOT EXISTS AccountTable(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
			account TEXT NOT NULL UNIQUE,
			password TEXT NOT NULL
        )
    )";
		db.update(createTableSQL);

		//如果没有visitor账号，插入visitor账号
		std::string insertSQL = R"(
        INSERT INTO 
        AccountTable (account, password)
        VALUES ('visitor', '0')
	)";
		bool result = db.update(insertSQL);
		if (result) {
			LOG("Visitor insert successful.")
		}
		else {
			LOG("Visitor has been insert")
		}

		//查询表中所有账号
		std::string selectSQL = R"(SELECT * FROM AccountTable)";
		int nrows, ncols;
		char** data = db.query(selectSQL, nrows, ncols);
		if (data) {
			for (int i = 0; i < nrows + 1; i++) {
				for (int j = 0; j < ncols; j++) {
					std::cout << data[i * ncols + j] << "  ";
				}
				std::cout << std::endl;
			}
			sqlite3_free_table(data);
		}
		else {
			LOG("Query failed.")
		}

		return true;
	}
}

bool B_Login::verifyAccount(std::string account, std::string password)
{
	//初始化
	LiteConn db("EmojiSystem");

	std::string selectSQL = R"(SELECT password FROM AccountTable WHERE account = '%s')";
	char temp[1000];
	strcpy(temp, selectSQL.data());
	sprintf(temp, selectSQL.data(), account.data());
	selectSQL = temp;

	int nrows, ncols;
	char** data = db.query(selectSQL, nrows, ncols);
	if (data) {
		if (nrows > 0 && strcmp(data[1], password.data()) == 0)
			return true;
		else
			return false;

		sqlite3_free_table(data);
	}
	else {

		LOG("Query failed.")
			return false;
	}
}

bool B_Login::registerAccount(std::string account, std::string password)
{
	//初始化
	LiteConn db("EmojiSystem");

	//插入账号密码
	std::string insertSQL = R"(
        INSERT INTO 
        AccountTable (account, password)
        VALUES ('%s', '%s')
	)";
	char temp[1000];
	strcpy(temp, insertSQL.data());
	sprintf(temp, insertSQL.data(), account.data(), password.data());
	insertSQL = temp;
	bool result = db.update(insertSQL);
	if (result) {
		LOG("Insert successful.")
			return true;
	}
	else {
		LOG("Insert failed.")
		return false;
	}
}