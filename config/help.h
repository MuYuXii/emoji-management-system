#pragma once
#include <iostream>

#define LOG(__STR__) \
    std::cout << "[" << __FILE__ << "] |in " << __LINE__ << " line |at " << __TIME__ << "] : " << __STR__ << std::endl;
