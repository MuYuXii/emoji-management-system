#include "Icon.h"

//构造函数
Icon::Icon(){}
Icon::Icon(IMAGE* icon_normal, IMAGE* icon_normal_mask, IMAGE* icon_select, IMAGE* icon_select_mask, int x_draw, int y_draw, RECT rect_select)
{
    this->distinction = None;
    status = Icon_Normal;
    this->icon_normal = icon_normal;
    this->icon_normal_mask = icon_normal_mask;
    this->icon_select = icon_select;
    this->icon_select_mask = icon_select_mask;
    this->x_draw = x_draw;
    this->y_draw = y_draw;
    this->rect_select = rect_select;
}
Icon::Icon(ButtonDistinction distinction, IMAGE* icon_normal, IMAGE* icon_normal_mask, IMAGE* icon_select, IMAGE* icon_select_mask, int x_draw, int y_draw, RECT rect_select)
{
    this->distinction = distinction;
    status = Icon_Normal;
    this->icon_normal = icon_normal;
    this->icon_normal_mask = icon_normal_mask;
    this->icon_select = icon_select;
    this->icon_select_mask = icon_select_mask;
    this->x_draw = x_draw;
    this->y_draw = y_draw;
    this->rect_select = rect_select;
}

//绘制图标函数
void Icon::DrawIcon()
{
    if (status == Icon_Normal)
    {
        putimage(x_draw, y_draw, icon_normal_mask, SRCAND);
        putimage(x_draw, y_draw, icon_normal, SRCPAINT);
    }
    else if (status == Icon_Select)
    {
        putimage(x_draw, y_draw, icon_select_mask, SRCAND);
        putimage(x_draw, y_draw, icon_select, SRCPAINT);
    }
}


//更新图标状态函数
void Icon::UpdateStatus(ExMessage msg)
{
    if (msg.x >= rect_select.left && msg.x <= rect_select.right && msg.y <= rect_select.bottom && msg.y >= rect_select.top)
    {
        if (this->status != Icon_Select)
            this->status = Icon_Select;
    }
    else
    {
        if (this->status != Icon_Normal)
            this->status = Icon_Normal;
    }
}

//判断图标是否被点击函数
bool Icon::IsClick(ExMessage msg)
{
    if (msg.x >= rect_select.left && msg.x <= rect_select.right && msg.y <= rect_select.bottom && msg.y >= rect_select.top)
    {
        if (msg.message == WM_LBUTTONDOWN)
            return true;
    }
    return false;
}
