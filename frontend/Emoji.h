#pragma once

#include<graphics.h>
#include<iostream>
#include"Ch.h"
#include"Icon.h"

enum EmojiStatus1 {
    none,//无

    addMenu//添加页面
};
enum EmojiStatus2 {
    isFavorite,//被喜欢

    isNotFavorite,//没有被喜欢
};

//表情包类
class Emoji
{
public:
    //构造函数
    Emoji();
    //构造函数
    Emoji(IMAGE* picture, char* name, IMAGE* favorite, IMAGE* notFavorite, IMAGE* del, int x_draw, int y_draw, RECT rect_select, int isLike);
    
    //--外部调用--
    //绘制表情包函数
    void DrawEmoji();
    //更新Emoji状态函数
    void UpdateEmjStatus(ExMessage msg);
    //判断删除按钮是否被点击函数
    bool DeleteIsClick(ExMessage msg);

    //--内部调用--
    //判断喜欢按钮是否被点击函数
    bool LikeIsClick(ExMessage msg);



    IMAGE* picture;//表情包资源
    IMAGE* notFavorite;//未被喜欢图标
    IMAGE* favorite;//已被喜欢图标
    IMAGE* del;     //删除图标

    EmojiStatus2 isLikeStatus;//Emoji是否被喜欢状态
    EmojiStatus1 isChangeStatus;//Emoji是否更改状态

    Ch* name;//表情包名称

    int x_draw;//绘制x坐标
    int y_draw;//绘制y坐标
    RECT rect_select;//选中区域
};