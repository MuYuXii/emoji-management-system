#include"Ch.h"
#include"Front.h"

//构造函数
Ch::Ch(){}
Ch::Ch(const char str[20], COLORREF color, RECT rect_draw, RECT rect_select, int height, int width)
{
	this->distinction = None;
	strcpy(this->str, str);
	this->color = color;
	this->rect_draw = rect_draw;
	this->rect_select = rect_select;
	this->height = height;
	this->width = width;
}
Ch::Ch(ButtonDistinction distinction, const char str[20], COLORREF color, RECT rect_draw, RECT rect_select, int height, int width)
{
	this->distinction = distinction;
    strcpy(this->str, str);
    this->color = color;
    this->rect_draw = rect_draw;
    this->rect_select = rect_select;
    this->height = height;
    this->width = width;
}

//绘制文字函数
void Ch::DrawChar()
{
    setbkmode(TRANSPARENT);
    settextstyle(height, width, "微软雅黑");
    settextcolor(this->color);
    drawtext(this->str, &this->rect_draw, DT_LEFT);
}

//更新颜色函数
void Ch::UpdateColor(ExMessage msg)
{
    if (msg.x >= rect_select.left && msg.x <= rect_select.right && msg.y <= rect_select.bottom && msg.y >= rect_select.top)
    {
        if (this->color != COLOR_SELECT)
            this->color = COLOR_SELECT;
    }
    else
    {
        if (this->color != COLOR_NORMAL)
            this->color = COLOR_NORMAL;
    }
}

//判断文字是否被点击函数
bool Ch::IsClick(ExMessage msg)
{
    if (msg.x >= rect_select.left && msg.x <= rect_select.right && msg.y <= rect_select.bottom && msg.y >= rect_select.top)
    {
        if (msg.message == WM_LBUTTONDOWN)
            return true;
    }
    return false;
}

//更改文字函数
void Ch::ChangeChar(Front* interf)
{
	int fps = 60;
	//光标左侧字符串
	char leftStr[1000] = { 0 };
	strcpy(leftStr, this->str);
	int leftStrCount = strlen(leftStr);
	//光标右侧字符串
	char rightStr[1000] = { 0 };
	int rightStrCount = 0;
	//光标
	char cursor = '|';
	int cursorCount = 0;
	//输入反馈字符串
	char feedbackStr[1001] = { 0 };
	//字符串消息
	ExMessage msg;
	//开始计数，结束计数，计数器频率
	LARGE_INTEGER startCount, endCount, F;

	//获取频率
	QueryPerformanceFrequency(&F);

	timeBeginPeriod(1);
	while (1)
	{
		//获取开始计数
		QueryPerformanceCounter(&startCount);


		//---绘制---
		//绘制背景
		interf->Draw(this->rect_draw);
		//设定文字样式
		settextcolor(BLACK);
		settextstyle(40, 0, "微软雅黑");
		//将 光标左侧字符串、光标、光标右侧字符串 进行拼接并绘制
		sprintf(feedbackStr, "%s%c%s", leftStr, cursor, rightStr);
		drawtext(feedbackStr, &rect_draw, DT_LEFT);

		//---帧率控制及接受输入---
		//获取结束计数
		QueryPerformanceCounter(&endCount);
		//计算时差，（起始计数-结束计数）/ 计数频率得到秒，乘以1000000得到微秒
		long long elapse = (endCount.QuadPart - startCount.QuadPart) * 1000000 / F.QuadPart;
		//时差大于1000000/60微秒（1/60秒）时跳出循环
		while (elapse < 1000000 / fps)
		{
			Sleep(1);

			if (peekmessage(&msg, EX_CHAR | EX_KEY))
			{
				char ch = msg.ch;
				//过滤键盘按下和弹起时传入的按键消息，仅接受字符消息（输入文字）
				if (msg.message != WM_KEYDOWN && msg.message != WM_KEYUP)
				{
					if (ch == '\r')
					{
						timeEndPeriod(1);
						char ret[2000] = { 0 };
						sprintf(ret, "%s%s", leftStr, rightStr);
						strcpy(this->str, ret);
						return;
					}
					else if (ch == '\b')
					{
						if (leftStrCount <= 0)
						{
							cursorCount = fps;
							continue;
						}
						//判断是否为中文，如果是中文，删除两个字符
						else if (leftStr[leftStrCount - 1] <= 128 && leftStr[leftStrCount - 1] >= 0)
						{
							cursorCount = fps;
							leftStr[--leftStrCount] = '\0';
						}
						else
						{
							cursorCount = fps;
							leftStr[--leftStrCount] = '\0';
							leftStr[--leftStrCount] = '\0';
						}
					}
					else
					{
						cursorCount = fps;
						leftStr[leftStrCount++] = ch;
					}
				}
				//过滤键盘弹起时传入的按键消息，仅接受键盘按下时传入的按键消息（移动光标）
				else if (msg.message != WM_KEYUP)
				{
					if (msg.vkcode == VK_LEFT && leftStrCount != 0)
					{
						cursorCount = fps;
						//判断是否为中文，若是，则移动两个char
						int count = 0;
						if (leftStr[leftStrCount - 1] <= 128 && leftStr[leftStrCount - 1] >= 0)
							count = 1;
						else
							count = 2;
						for (int j = 0; j < count; j++)
						{
							for (int i = rightStrCount; i > 0; i--)
								rightStr[i] = rightStr[i - 1];
							rightStr[0] = leftStr[leftStrCount - 1];
							leftStr[leftStrCount - 1] = '\0';
							leftStrCount--;
							rightStrCount++;
						}
					}
					else if (msg.vkcode == VK_RIGHT && rightStrCount != 0)
					{
						cursorCount = fps;
						//判断是否为中文，若是，则移动两个char
						int count = 0;
						if (rightStr[0] <= 128 && rightStr[0] >= 0)
							count = 1;
						else
							count = 2;
						for (int j = 0; j < count; j++)
						{
							leftStr[leftStrCount] = rightStr[0];
							for (int i = 0; i < rightStrCount; i++)
								rightStr[i] = rightStr[i + 1];
							leftStrCount++;
							rightStrCount--;
						}
					}
				}
			}
			//计算当前时差
			QueryPerformanceCounter(&endCount);
			elapse = (endCount.QuadPart - startCount.QuadPart) * 1000000 / F.QuadPart;
		}


		//---光标闪烁---
		//光标计数器在[0,15)时，光标显示
		if (cursorCount == fps / 2)
		{
			cursor = ' ';
		}
		//光标计数器在[15,30)时，光标消失
		else if (cursorCount >= fps)
		{
			cursor = '|';
			cursorCount = 0;
		}
		cursorCount++;

		FlushBatchDraw();
	}
}