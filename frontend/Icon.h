#pragma once

#include <graphics.h>
#include<iostream>
#include"Ch.h"

enum IconStatus
{
    //
    Icon_Normal,

    //
    Icon_Select
};

//图标类
class Icon
{
public:
    //构造函数
    Icon();
    
    Icon(IMAGE* icon_normal, IMAGE* icon_normal_mask, IMAGE* icon_select, IMAGE* icon_select_mask, int x_draw, int y_draw, RECT rect_select);
    
    Icon(ButtonDistinction distinction, IMAGE* icon_normal, IMAGE* icon_normal_mask, IMAGE* icon_select, IMAGE* icon_select_mask, int x_draw, int y_draw, RECT rect_select);
    //绘制图标函数
    void DrawIcon();
    //更新图标状态函数
    void UpdateStatus(ExMessage msg);
    //判断图标是否被点击函数
    bool IsClick(ExMessage msg);
    
    ButtonDistinction distinction;//转跳目的地

    IconStatus status;//图标状态

    IMAGE* icon_normal;//普通状态图标
    IMAGE* icon_normal_mask;//普通状态图标掩码
    IMAGE* icon_select;//选中状态图标
    IMAGE* icon_select_mask;//选中状态图标掩码

    int x_draw;//绘制x坐标
    int y_draw;//绘制y坐标
    RECT rect_select;//选中区域
};