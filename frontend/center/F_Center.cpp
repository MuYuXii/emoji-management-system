#include "F_Center.h"

F_Center::F_Center(string Account)
{
	this->Account = Account;

	loadimage(&interfaceImg, "./frontend/assets/centerInterface.png");

	pages = 0;

	loadimage(&kuangkuang, "./frontend/assets/kuangkuang.png");
	loadimage(&kuangkuang_mask, "./frontend/assets/kuangkuang_mask.png");

	//---文字变量初始化---
	RECT r_draw, r_select;
	Ch* ch;
	ch = new(Ch);
	r_draw = { 650, 70, 1000, 105 }, r_select = { 650, 70, 1000, 110 };
	*ch = Ch("", COLOR_NORMAL, r_draw, r_select, 40);
	inputStr = ch;

	//用户账号 提示文字 初始化
	ch = new(Ch);
	r_draw = { 165, 60, 550, 100 }, r_select = { 0 };
	char tempStr[100] = { 0 };
	sprintf(tempStr, "用户：%s", Account.data());
	*ch = Ch(tempStr, COLOR_NORMAL, r_draw, r_select, 30);
	hintStr.push_back(ch);

	//图标 初始化
	int x_draw, y_draw;
	Icon* icon;

	//---转跳界面按钮图标 初始化---
	//返回 图标
	icon = new(Icon);
	x_draw = 75, y_draw = 74, r_select = { 75, 75 , 75 + 52, 74 + 56 };
	IMAGE* icon_normal = new(IMAGE);
	IMAGE* icon_normal_mask = new(IMAGE);
	IMAGE* icon_select = new(IMAGE);
	IMAGE* icon_select_mask = new(IMAGE);
	loadimage(icon_normal, "./frontend/assets/center_icon_back.png");
	loadimage(icon_normal_mask, "./frontend/assets/center_icon_back_mask.png");
	loadimage(icon_select, "./frontend/assets/center_icon_back_select.png");
	loadimage(icon_select_mask, "./frontend/assets/center_icon_back_select_mask.png");
	*icon = Icon(Start, icon_normal, icon_normal_mask, icon_select, icon_select_mask, x_draw, y_draw, r_select);
	quitButtonIcon.push_back(icon);

	//---界面内数据更新按钮 初始化---
	//添加 图标
	icon = new(Icon);
	x_draw = 75, y_draw = 174, r_select = { 75, 174 , 75 + 60, 174 + 60 };
	icon_normal = new(IMAGE);
	icon_normal_mask = new(IMAGE);
	icon_select = new(IMAGE);
	icon_select_mask = new(IMAGE);
	loadimage(icon_normal, "./frontend/assets/center_icon_add.png");
	loadimage(icon_normal_mask, "./frontend/assets/center_icon_add_mask.png");
	loadimage(icon_select, "./frontend/assets/center_icon_add_select.png");
	loadimage(icon_select_mask, "./frontend/assets/center_icon_add_select_mask.png");
	*icon = Icon(icon_normal, icon_normal_mask, icon_select, icon_select_mask, x_draw, y_draw, r_select);
	updateIcon.push_back(icon);

	//我的表情包 图标
	icon = new(Icon);
	x_draw = 75, y_draw = 274, r_select = { 75, 274 , 75 + 52, 274 + 56 };
	icon_normal = new(IMAGE);
	icon_normal_mask = new(IMAGE);
	icon_select = new(IMAGE);
	icon_select_mask = new(IMAGE);
	loadimage(icon_normal, "./frontend/assets/center_icon_myEmoji.png");
	loadimage(icon_normal_mask, "./frontend/assets/center_icon_myEmoji_mask.png");
	loadimage(icon_select, "./frontend/assets/center_icon_myEmoji_select.png");
	loadimage(icon_select_mask, "./frontend/assets/center_icon_myEmoji_select_mask.png");
	*icon = Icon(icon_normal, icon_normal_mask, icon_select, icon_select_mask, x_draw, y_draw, r_select);
	updateIcon.push_back(icon);

	//喜欢 图标
	icon = new(Icon);
	x_draw = 75, y_draw = 374, r_select = { 75, 374 , 75 + 60, 374 + 60 };
	icon_normal = new(IMAGE);
	icon_normal_mask = new(IMAGE);
	icon_select = new(IMAGE);
	icon_select_mask = new(IMAGE);
	loadimage(icon_normal, "./frontend/assets/center_icon_like.png");
	loadimage(icon_normal_mask, "./frontend/assets/center_icon_like_mask.png");
	loadimage(icon_select, "./frontend/assets/center_icon_like_select.png");
	loadimage(icon_select_mask, "./frontend/assets/center_icon_like_select_mask.png");
	*icon = Icon(icon_normal, icon_normal_mask, icon_select, icon_select_mask, x_draw, y_draw, r_select);
	updateIcon.push_back(icon);

	//上一页 图标
	icon = new(Icon);
	x_draw = 1000, y_draw = 130, r_select = { 1000, 130 , 1000 + 60, 130 + 60 };
	icon_normal = new(IMAGE);
	icon_normal_mask = new(IMAGE);
	icon_select = new(IMAGE);
	icon_select_mask = new(IMAGE);
	loadimage(icon_normal, "./frontend/assets/center_icon_lastPage.png");
	loadimage(icon_normal_mask, "./frontend/assets/center_icon_lastPage_mask.png");
	loadimage(icon_select, "./frontend/assets/center_icon_lastPage_select.png");
	loadimage(icon_select_mask, "./frontend/assets/center_icon_lastPage_select_mask.png");
	*icon = Icon(icon_normal, icon_normal_mask, icon_select, icon_select_mask, x_draw, y_draw, r_select);
	updateIcon.push_back(icon);

	//下一页 图标
	icon = new(Icon);
	x_draw = 1000, y_draw = 590, r_select = { 1000, 590 , 1000 + 60, 590 + 60 };
	icon_normal = new(IMAGE);
	icon_normal_mask = new(IMAGE);
	icon_select = new(IMAGE);
	icon_select_mask = new(IMAGE);
	loadimage(icon_normal, "./frontend/assets/center_icon_nextPage.png");
	loadimage(icon_normal_mask, "./frontend/assets/center_icon_nextPage_mask.png");
	loadimage(icon_select, "./frontend/assets/center_icon_nextPage_select.png");
	loadimage(icon_select_mask, "./frontend/assets/center_icon_nextPage_select_mask.png");
	*icon = Icon(icon_normal, icon_normal_mask, icon_select, icon_select_mask, x_draw, y_draw, r_select);
	updateIcon.push_back(icon);

	//---表情包数组 初始化---
	//获取表情包地址数组
	//-------依赖后端
	/*char emojiPath[100][100] = { 0 };
	int nrow = 42, ncol = 2;
	for (int i = ncol * 1; i < nrow * ncol + 2; i++)
	{
		if (i % 2 == 0)
		{
			char a[100] = { 0 };
			sprintf(a, "%d", i / 2);
			strcpy(emojiPath[i], a);
		}
		else
		{
			char a[100] = { 0 };
			sprintf(a, "./picture/%d.jpg", i / 2);
			strcpy(emojiPath[i], a);
		}
	}*/
	//------依赖后端

	int nrow, ncol;
	char** emojiPath = b_center.searchEmojiByID(this->Account, nrow, ncol);

	//Emoji(IMAGE * picture, char* name, IMAGE * favorite, IMAGE * notFavorite, IMAGE * del, int x_draw, int y_draw, RECT rect_select);
	for (int i = ncol * 1; i < nrow * ncol + ncol; i += ncol)
	{
		Emoji* emo = new Emoji;

		int x_draw = 0, y_draw = 0;
		RECT rect_select = { 0 };
		IMAGE* picture = new IMAGE;
		IMAGE* favorite = new IMAGE;
		IMAGE* notFavorite = new IMAGE;
		IMAGE* del = new IMAGE;
		loadimage(picture, emojiPath[i + 1], 150, 150);
		loadimage(favorite, "./frontend/assets/favorite.png", 75, 50, true);
		loadimage(notFavorite, "./frontend/assets/notFavorite.png", 75, 50, true);
		loadimage(del, "./frontend/assets/del.png", 75, 50, true);

		*emo = Emoji(picture, emojiPath[i], favorite, notFavorite, del, x_draw, y_draw, rect_select, emojiPath[i + 2][0] - '0');
		emoji.push_back(emo);
	}

	sqlite3_free_table(emojiPath);

	resetEmojiPosition();
}

F_Center::~F_Center()
{
	//释放提示文字内存
	for (vector<Ch*>::iterator it = hintStr.begin(); it != hintStr.end(); it++)
	{
		delete (*it);
	}

	//释放输入文字内存
	delete inputStr;

	//释放按钮图标内存
	for (vector<Icon*>::iterator it = quitButtonIcon.begin(); it != quitButtonIcon.end(); it++)
	{
		delete (*it)->icon_normal;
		delete (*it)->icon_normal_mask;
		delete (*it)->icon_select;
		delete (*it)->icon_select_mask;
		delete (*it);
	}

	for (vector<Icon*>::iterator it = updateIcon.begin(); it != updateIcon.end(); it++)
	{
		delete (*it)->icon_normal;
		delete (*it)->icon_normal_mask;
		delete (*it)->icon_select;
		delete (*it)->icon_select_mask;
		delete (*it);
	}

	//释放表情包内存
	for (vector<Emoji*>::iterator it = emoji.begin(); it != emoji.end(); it++)
	{
		delete (*it)->picture;
		delete (*it)->favorite;
		delete (*it)->notFavorite;
		delete (*it)->del;
		delete (*it)->name;
		delete (*it);
	}
}

void F_Center::Draw(RECT notDrawRect)
{
	//绘制背景图
	putimage(0, 0, &interfaceImg);

	//绘制输入文字
	inputStr->DrawChar();
	//绘制提示文字
	for (int i = 0; i < hintStr.size(); i++)
	{
		hintStr[i]->DrawChar();
	}

	//绘制数据更新图标
	for (int i = 0; i < updateIcon.size(); i++)
	{
		updateIcon[i]->DrawIcon();
	}

	//绘制界面
	for (int i = 0; i < quitButtonIcon.size(); i++)
	{
		quitButtonIcon[i]->DrawIcon();
	}

	for (int i = pages * 12; i < pages * 12 + 12 && i < emoji.size(); i++)
	{
		emoji[i]->DrawEmoji();
	}
}

void F_Center::Update(ExMessage msg)
{
	//更新 输入文字
	if (inputStr->IsClick(msg))
	{
		inputStr->ChangeChar(this);
		int is = 1;
		for (int i = 0; i < emoji.size(); i++)
		{
			if (strcmp(emoji[i]->name->str, inputStr->str) == 0)
			{
				int j = i % 12;
				is = 0;
				strcpy(inputStr->str, "");
				pages = i / 12;
				this->Draw();
				putimage(187 + 200 * (j % 4) - 10, 125 + 170 * (j / 4) - 10, &kuangkuang_mask, SRCAND);
				putimage(187 + 200 * (j % 4) - 10, 125 + 170 * (j / 4) - 10, &kuangkuang, SRCPAINT);
				FlushBatchDraw();
				ExMessage msg1;
				while (1)
				{
					if (peekmessage(&msg1, EX_MOUSE))
					{
						if (msg1.message == WM_LBUTTONDOWN)
						{
							break;
						}
					}
				}
			}
		}
		if (is)
		{
			strcpy(inputStr->str, "");
			this->Draw();
			setbkmode(TRANSPARENT);
			settextstyle(30, 0, "微软雅黑");
			settextcolor(RED);
			RECT errorRect = { 0,0,500,100 };
			drawtext("没有此表情包！", &errorRect, DT_LEFT);
			FlushBatchDraw();
			ExMessage msg1;
			while (1)
			{
				if (peekmessage(&msg1, EX_MOUSE))
				{
					if (msg1.message == WM_LBUTTONDOWN)
					{
						break;
					}
				}
			}
		}
	}

	//更新 提示文字
	for (int i = 0; i < hintStr.size(); i++)
	{
		if (i == 0)
		{
			char tempStr[100] = { 0 };
			sprintf(tempStr, "用户：%s", Account.data());
			if (hintStr[i]->str != tempStr)
			{
				strcpy(hintStr[i]->str, tempStr);
			}
		}
		hintStr[i]->UpdateColor(msg);
	}

	//更新 数据更新图标
	for (int i = 0; i < updateIcon.size(); i++)
	{
		//翻页
		updateIcon[i]->UpdateStatus(msg);
		if (updateIcon[i]->x_draw == 1000 && updateIcon[i]->y_draw == 130)
		{
			if (pages > 0 && updateIcon[i]->IsClick(msg))
			{
				pages--;
			}
		}
		if (updateIcon[i]->x_draw == 1000 && updateIcon[i]->y_draw == 590)
		{
			if (pages < (emoji.size() - 1) / 12 && updateIcon[i]->IsClick(msg))
			{
				pages++;
			}
		}

		//切换至喜欢界面
		if (updateIcon[i]->x_draw == 75 && updateIcon[i]->y_draw == 374)
		{
			if (updateIcon[i]->IsClick(msg))
			{
				for (vector<Emoji*>::iterator it = emoji.begin(); it != emoji.end(); it++)
				{
					delete (*it)->picture;
					delete (*it)->favorite;
					delete (*it)->notFavorite;
					delete (*it)->del;
					delete (*it)->name;
					delete (*it);
				}
				emoji.clear();

				reinLike();
			}
		}

		//切换至我的表情包界面
		if (updateIcon[i]->x_draw == 75 && updateIcon[i]->y_draw == 274)
		{
			if (updateIcon[i]->IsClick(msg))
			{
				for (vector<Emoji*>::iterator it = emoji.begin(); it != emoji.end(); it++)
				{
					delete (*it)->picture;
					delete (*it)->favorite;
					delete (*it)->notFavorite;
					delete (*it)->del;
					delete (*it)->name;
					delete (*it);
				}
				emoji.clear();

				rein();
			}
		}

		//添加表情包
		if (updateIcon[i]->x_draw == 75 && updateIcon[i]->y_draw == 174)
		{
			if (updateIcon[i]->IsClick(msg))
			{
				//---命名---
				int maxNum = b_center.searchBigEmojiName();
				char addStr[50] = { 0 };
				sprintf(addStr, "%d.jpg", maxNum + 1);

				//---拷贝---
				//获取文件的路径
				GetFilePath getfilepath;
				string str = getfilepath.selectFile();
				cout << str << endl;
				// 源文件的绝对路径
				char sourcePath[500] = { 0 };
				strcpy(sourcePath, str.data());
				// 目标文件夹的绝对路径
				char path1[100] = { 0 };
				DWORD pathLength = GetCurrentDirectoryA(MAX_PATH, path1);
				char targetFolder[500];
				sprintf(targetFolder, "%s\\picture", path1);
				// 目标文件的完整路径
				char targetPath[MAX_PATH];
				GetFullPathNameA(targetFolder, MAX_PATH, targetPath, NULL);
				strcat(targetPath, "\\");
				strcat(targetPath, addStr); // 假设我们知道源文件的文件名
				// 复制文件
				if (CopyFileA(sourcePath, targetPath, FALSE))
				{
					//---写入数据库---
					string emojiName = to_string(maxNum + 1);
					string accountId = Account;
					string AddStr = addStr;
					string Path = "./picture/" + AddStr;
					b_center.addEmoji(Path, accountId, emojiName);
					rein();
				}

			}
		}
	}


	//更新 界面转跳图标
	for (int i = 0; i < quitButtonIcon.size(); i++)
	{
		quitButtonIcon[i]->UpdateStatus(msg);
	}


	//更新 表情包
	for (int i = pages * 12; i < pages * 12 + 12 && i < emoji.size(); i++)
	{
		emoji[i]->UpdateEmjStatus(msg);

		//删除表情包
		if (emoji[i]->DeleteIsClick(msg))
		{
			string Name = emoji[i]->name->str;
			b_center.deleteEmoji(Name, this->Account);

			DeleteEmoji(i);
			resetEmojiPosition();
			continue;
		}

		//将该表情包设为喜欢或取消喜欢
		if (emoji[i]->LikeIsClick(msg))
		{
			string Name = emoji[i]->name->str;
			int isLike = 0;
			if (emoji[i]->isLikeStatus == isFavorite)
				isLike = 1;
			b_center.updateEmoji(Name, this->Account, isLike);
		}
	}
}

ButtonDistinction F_Center::Quit(ExMessage msg)
{
	//判断 转跳界面按钮图标 的退出转跳指令
	for (vector<Icon*>::iterator it = quitButtonIcon.begin(); it != quitButtonIcon.end(); it++)
	{
		if ((*it)->IsClick(msg))
		{
			//退出程序
			if ((*it)->distinction == QuitSystem)
				return QuitSystem;
			//转跳登录界面
			if ((*it)->distinction == Login)
				return Login;
			//转跳开始界面
			if ((*it)->distinction == Start)
				return Start;
			//转跳注册界面
			if ((*it)->distinction == Regist)
				return Regist;
			//转跳找回密码界面
			if ((*it)->distinction == Retrieve)
				return Retrieve;
			//转跳主界面
			if ((*it)->distinction == Center)
				return Center;
		}
	}
}






void F_Center::DeleteEmoji(int x)
{
	delete emoji[x]->picture;
	delete emoji[x]->favorite;
	delete emoji[x]->notFavorite;
	delete emoji[x]->del;
	delete emoji[x]->name;
	delete emoji[x];
	for (int i = x; i < emoji.size() - 1; i++)
		emoji[i] = emoji[i + 1];

	emoji.pop_back();
}

//重置所有表情包位置信息
void F_Center::resetEmojiPosition()
{
	for (int i = 0; i < emoji.size(); i++)
	{
		int j = i % 12;
		emoji[i]->x_draw = 187 + 200 * (j % 4);
		emoji[i]->y_draw = 125 + 170 * (j / 4);
		emoji[i]->rect_select = { emoji[i]->x_draw, emoji[i]->y_draw, emoji[i]->x_draw + 150, emoji[i]->y_draw + 150 };
		emoji[i]->name->rect_draw = { emoji[i]->rect_select.left + 70, emoji[i]->rect_select.bottom, emoji[i]->rect_select.right, emoji[i]->rect_select.bottom + 20 };
	}
}

void F_Center::rein()
{
	emoji.clear();
	int nrow, ncol;
	char** emojiPath = b_center.searchEmojiByID(this->Account, nrow, ncol);

	//Emoji(IMAGE * picture, char* name, IMAGE * favorite, IMAGE * notFavorite, IMAGE * del, int x_draw, int y_draw, RECT rect_select);
	for (int i = ncol * 1; i < nrow * ncol + ncol; i += ncol)
	{
		Emoji* emo = new Emoji;

		int x_draw = 0, y_draw = 0;
		RECT rect_select = { 0 };
		IMAGE* picture = new IMAGE;
		IMAGE* favorite = new IMAGE;
		IMAGE* notFavorite = new IMAGE;
		IMAGE* del = new IMAGE;
		loadimage(picture, emojiPath[i + 1], 150, 150);
		loadimage(favorite, "./frontend/assets/favorite.png", 75, 50, true);
		loadimage(notFavorite, "./frontend/assets/notFavorite.png", 75, 50, true);
		loadimage(del, "./frontend/assets/del.png", 75, 50, true);

		*emo = Emoji(picture, emojiPath[i], favorite, notFavorite, del, x_draw, y_draw, rect_select, emojiPath[i + 2][0] - '0');
		emoji.push_back(emo);
	}

	sqlite3_free_table(emojiPath);

	resetEmojiPosition();
}

void F_Center::reinLike()
{
	emoji.clear();
	int nrow, ncol;
	char** emojiPath = b_center.searchEmojiByID(this->Account, nrow, ncol);

	//Emoji(IMAGE * picture, char* name, IMAGE * favorite, IMAGE * notFavorite, IMAGE * del, int x_draw, int y_draw, RECT rect_select);
	for (int i = ncol * 1; i < nrow * ncol + ncol; i += ncol)
	{
		if (emojiPath[i + 2][0] == '1')

		{
			Emoji* emo = new Emoji;
			int x_draw = 0, y_draw = 0;
			RECT rect_select = { 0 };
			IMAGE* picture = new IMAGE;
			IMAGE* favorite = new IMAGE;
			IMAGE* notFavorite = new IMAGE;
			IMAGE* del = new IMAGE;
			loadimage(picture, emojiPath[i + 1], 150, 150);
			loadimage(favorite, "./frontend/assets/favorite.png", 75, 50, true);
			loadimage(notFavorite, "./frontend/assets/notFavorite.png", 75, 50, true);
			loadimage(del, "./frontend/assets/del.png", 75, 50, true);

			*emo = Emoji(picture, emojiPath[i], favorite, notFavorite, del, x_draw, y_draw, rect_select, emojiPath[i + 2][0] - '0');
			emoji.push_back(emo);
		}
	}

	sqlite3_free_table(emojiPath);

	resetEmojiPosition();
}
