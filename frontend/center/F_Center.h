#pragma once
#ifndef __F_CENTER__
#define __F_CENTER__

#include "../Front.h"
#include "../../config/help.h"
#include "../Emoji.h"
#include "../../backend/center/GetFilePath.h"
#include"../../backend/center/B_Center.h"

enum centerStatus
{
    normal,
    like
};


class F_Center :public Front {
public:
    F_Center(string Account);
    ~F_Center();
    void Draw(RECT notDrawRect = { 0 });//绘制函数
    void Update(ExMessage msg);//更新函数
    ButtonDistinction Quit(ExMessage msg);//退出函数（可重写)


    void DeleteEmoji(int x);
    void resetEmojiPosition();
    void rein();
    void reinLike();

private:
    //界面图
    IMAGE interfaceImg;

    //主界面后端
    B_Center b_center;

    //搜索框文字
    Ch* inputStr;
    //提示文字数组
    vector<Ch*>hintStr;
    
    //界面内数据更新按钮数组
    vector<Icon*>updateIcon;
    //转跳界面按钮数组
    vector<Icon*>quitButtonIcon;

    //表情包数组
    vector<Emoji*>emoji;

    //主界面页面状态
    centerStatus center_status;

    //当前页数
    int pages;

    //框框
    IMAGE kuangkuang;
    IMAGE kuangkuang_mask;
};

#endif