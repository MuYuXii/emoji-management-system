#pragma once

#include <graphics.h>
#include<iostream>

//防止循环包含提前声明
class Front;

class Ch;

//按钮目的地枚举
enum ButtonDistinction
{
    None,//无
    QuitSystem,//退出程序
    Start,//开始界面
    Login,//登录界面
    Regist,//注册界面
    Retrieve,//找回密码界面,
    Center,//操作界面

    favorite,//喜欢图标
    delEmoji,//删除按钮
    preciousPage,//翻页按钮（上）
    nextPage,//翻页按钮（下）
};

//文字颜色
#define            COLOR_NORMAL        BLACK       //普通状态颜色
#define            COLOR_SELECT        RED         //选中状态颜色

//文字类
class Ch
{
public:
    //构造函数
    Ch();
    Ch(const char str[20], COLORREF color, RECT rect_draw, RECT rect_select, int height, int width = 0);
    Ch(ButtonDistinction flag, const char str[20], COLORREF color, RECT rect_draw, RECT rect_select, int height, int width = 0);
    //绘制文字函数
    void DrawChar();
    //更新颜色函数
    void UpdateColor(ExMessage msg);
    //判断文字是否被点击函数
    bool IsClick(ExMessage msg);
    //更改文字函数
    void ChangeChar(Front* interf);

    ButtonDistinction distinction;//转跳目的地
    char str[20];//文字
    COLORREF color;//文字颜色
    RECT rect_draw;//绘制区域
    RECT rect_select;//选中区域
    int height;//文字高度
    int width;//文字宽度
};