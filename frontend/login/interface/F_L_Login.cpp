#include "F_L_Login.h"

//构造函数
F_L_Login::F_L_Login()
{
	//---加载界面图---
	loadimage(&interfaceImg, "./frontend/assets/LoginInterface.png");


	//---文字变量初始化---
	RECT r_draw, r_select;
	Ch* ch;

	//账号 提示文字 初始化
	ch = new(Ch);
	r_draw = { 309, 205, 397, 249 }, r_select = { 400,200, 800, 250 };
	*ch = Ch("账号:", COLOR_NORMAL, r_draw, r_select, 40);
	hintStr.push_back(ch);

	//账号 输入文字 初始化
	ch = new(Ch);
	*ch = Ch("", COLOR_NORMAL, r_select, r_select, 40);
	inputStr.push_back(ch);
	
	//密码 提示文字 初始化
	ch = new(Ch);
	r_draw = { 309, 303, 400, 345 }, r_select = { 400,300,800,350 };
	*ch = Ch("密码:", COLOR_NORMAL, r_draw, r_select, 40);
	hintStr.push_back(ch);

	//密码 输入文字 初始化
	ch = new(Ch);
	*ch = Ch("", COLOR_NORMAL, r_select, r_select, 40);
	inputStr.push_back(ch);

	//验证码 提示文字 初始化
	ch = new(Ch);
	r_draw = { 280, 403,400,445 }, r_select = { 400,400,650,450 };
	*ch = Ch("验证码:", COLOR_NORMAL, r_draw, r_select, 40);
	hintStr.push_back(ch);

	//验证码 输入文字 初始化
	ch = new(Ch);
	*ch = Ch("", COLOR_NORMAL, r_select, r_select, 40);
	inputStr.push_back(ch);

	//验证码的四个数字
	int left = 15, right = 50;
	srand((unsigned)time(NULL));
	char num = rand() % 10;
	int size = rand() % (right - left + 1) + left;
	string strNum;
	strNum.push_back(num + '0');
	ch = new(Ch);
	r_draw = { 675,400,800,450 }, r_select = { 0 };
	*ch = Ch(strNum.data(), COLOR_NORMAL, r_draw, r_select, size);
	strNum.pop_back();
	hintStr.push_back(ch);

	num = rand() % 10;
	size = rand() % (right - left + 1) + left;
	strNum.push_back(num + '0');
	ch = new(Ch);
	r_draw = { 705,400,800,450 }, r_select = { 0 };
	*ch = Ch(strNum.data(), COLOR_NORMAL, r_draw, r_select, size);
	strNum.pop_back();
	hintStr.push_back(ch);

	num = rand() % 10;
	size = rand() % (right - left + 1) + left;
	strNum.push_back(num + '0');
	ch = new(Ch);
	r_draw = { 735,400,800,450 }, r_select = { 0 };
	*ch = Ch(strNum.data(), COLOR_NORMAL, r_draw, r_select, size);
	strNum.pop_back();
	hintStr.push_back(ch);

	num = rand() % 10;
	size = rand() % (right - left + 1) + left;
	strNum.push_back(num + '0');
	ch = new(Ch);
	r_draw = { 765,400,800,450 }, r_select = { 0 };
	*ch = Ch(strNum.data(), COLOR_NORMAL, r_draw, r_select, size);
	strNum.pop_back();
	hintStr.push_back(ch);


	//登录 按钮文字 初始化
	ch = new(Ch);
	r_draw = { 540,503,800,550 }, r_select = { 400, 500, 800, 550 };
	*ch = Ch(Center,"登      录", COLOR_NORMAL, r_draw, r_select, 40);
	buttonStr.push_back(ch);

	//注册 按钮文字 初始化
	ch = new(Ch);
	r_draw = { 550, 560, 800, 800 }, r_select = { 550, 560, 665, 586 };
	*ch = Ch(Regist, "新用户注册", COLOR_NORMAL, r_draw, r_select, 25);
	buttonStr.push_back(ch);

	//---图标变量初始化---
	int x_draw, y_draw;
	Icon* icon;

	icon = new(Icon);
	x_draw = 0, y_draw = 0, r_select = { 1, 1 , 40, 40 };
	IMAGE* icon_normal = new(IMAGE);
	IMAGE* icon_normal_mask = new(IMAGE);
	IMAGE* icon_select = new(IMAGE);
	IMAGE* icon_select_mask = new(IMAGE);
	loadimage(icon_normal, "./frontend/assets/icon_back.png");
	loadimage(icon_normal_mask, "./frontend/assets/icon_back_mask.png");
	loadimage(icon_select, "./frontend/assets/icon_back_select.png");
	loadimage(icon_select_mask, "./frontend/assets/icon_back_select_mask.png");
	*icon = Icon(Start, icon_normal, icon_normal_mask, icon_select, icon_select_mask, x_draw, y_draw, r_select);
	quitButtonIcon.push_back(icon);

	icon = new(Icon);
	x_draw = 1080, y_draw = 0, r_select = { 1080, 0, 1120, 40 };
	icon_normal = new(IMAGE);
	icon_normal_mask = new(IMAGE);
	icon_select = new(IMAGE);
	icon_select_mask = new(IMAGE);
	loadimage(icon_normal, "./frontend/assets/icon_quit.png");
	loadimage(icon_normal_mask, "./frontend/assets/icon_quit_mask.png");
	loadimage(icon_select, "./frontend/assets/icon_quit_select.png");
	loadimage(icon_select_mask, "./frontend/assets/icon_quit_select_mask.png");
	*icon = Icon(QuitSystem, icon_normal, icon_normal_mask, icon_select, icon_select_mask, x_draw, y_draw, r_select);
	quitButtonIcon.push_back(icon);
}

//析构函数
F_L_Login::~F_L_Login()
{
	//释放提示文字内存
	for (vector<Ch*>::iterator it = hintStr.begin(); it != hintStr.end(); it++)
	{
		delete (*it);
	}
	//释放输入文字内存
	for (vector<Ch*>::iterator it = inputStr.begin(); it != inputStr.end(); it++)
	{
		delete (*it);
	}
	//释放按钮文字内存
	for (vector<Ch*>::iterator it = buttonStr.begin(); it != buttonStr.end(); it++)
	{
		delete (*it);
	}
	//释放按钮图标内存
	for (vector<Icon*>::iterator it = quitButtonIcon.begin(); it != quitButtonIcon.end(); it++)
	{
		delete (*it)->icon_normal;
		delete (*it)->icon_normal_mask;
		delete (*it)->icon_select;
		delete (*it)->icon_select_mask;
		delete (*it);
	}
}

//绘制函数在父类中已实现
//更新函数已在父类中实现
//重写退出函数

inline ButtonDistinction F_L_Login::Quit(ExMessage msg)
{
	//判断 转跳界面按钮文字 的退出转跳指令
	for (vector<Ch*>::iterator it = buttonStr.begin(); it != buttonStr.end(); it++)
	{
		if ((*it)->IsClick(msg))
		{
			//退出程序
			if ((*it)->distinction == QuitSystem)
				return QuitSystem;
			//转跳登录界面
			if ((*it)->distinction == Login)
				return Login;
			//转跳开始界面
			if ((*it)->distinction == Start)
				return Start;
			//转跳注册界面
			if ((*it)->distinction == Regist)
				return Regist;
			//转跳找回密码界面
			if ((*it)->distinction == Retrieve)
				return Retrieve;


			//重写部分
			//转跳登录界面，需验证密码和验证码
			if ((*it)->distinction == Center)
			{
				//---inputStr[0]->str为账号，inputStr[1]->str为密码，inputStr[2]->str为验证码---
				string verCode;
				for (int i = 3; i <= 6; i++)
					verCode.push_back(hintStr[i]->str[0]);
				if (verCode != inputStr[2]->str)
				{
					setbkmode(TRANSPARENT);
					settextstyle(30, 0, "微软雅黑");
					settextcolor(RED);
					RECT errorRect = { this->inputStr[2]->rect_select.right + 150, this->inputStr[2]->rect_select.top + 10, this->inputStr[2]->rect_select.right + 150 + 400,this->inputStr[2]->rect_select.bottom };
					drawtext("验证码错误！", &errorRect, DT_LEFT);
					FlushBatchDraw();
					while (1)
					{
						if (peekmessage(&msg, EX_MOUSE))
						{
							if (msg.message == WM_LBUTTONDOWN)
							{
								break;
							}
						}
					}
					return None;
				}
				else if (b_login.verifyAccount(inputStr[0]->str, inputStr[1]->str))
				{
					Account = inputStr[0]->str;
					return Center;
				}
				else
				{
					setbkmode(TRANSPARENT);
					settextstyle(30, 0, "微软雅黑");
					settextcolor(RED);
					RECT errorRect = { this->inputStr[0]->rect_select.right, this->inputStr[0]->rect_select.top + 10, this->inputStr[0]->rect_select.right + 400,this->inputStr[0]->rect_select.bottom };
					drawtext("密码错误或账号不存在！", &errorRect, DT_LEFT);
					FlushBatchDraw();
					while (1)
					{
						if (peekmessage(&msg, EX_MOUSE))
						{
							if (msg.message == WM_LBUTTONDOWN)
							{
								break;
							}
						}
					}
					return None;
				}

			}
		}
	}

	//判断 转跳界面按钮图标 的退出转跳指令
	for (vector<Icon*>::iterator it = quitButtonIcon.begin(); it != quitButtonIcon.end(); it++)
	{
		if ((*it)->IsClick(msg))
		{
			//退出程序
			if ((*it)->distinction == QuitSystem)
				return QuitSystem;
			//转跳登录界面
			if ((*it)->distinction == Login)
				return Login;
			//转跳开始界面
			if ((*it)->distinction == Start)
				return Start;
			//转跳注册界面
			if ((*it)->distinction == Regist)
				return Regist;
			//转跳找回密码界面
			if ((*it)->distinction == Retrieve)
				return Retrieve;
			//转跳主界面
			if ((*it)->distinction == Center)
			{
				Account = inputStr[0]->str;
				return Center;
			}

		}
	}
}



//绘制函数在父类中已实现

//更新函数已在父类中实现

