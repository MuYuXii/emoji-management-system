#pragma once

#include"../F_Login.h"
#include<time.h>

class F_L_Login : public F_Login
{
public:
	F_L_Login();//构造函数
	~F_L_Login();//析构函数

	//绘制函数在父类中已实现
	//更新函数已在父类中实现
	//重写退出函数
	ButtonDistinction Quit(ExMessage msg);//重写退出函数

};