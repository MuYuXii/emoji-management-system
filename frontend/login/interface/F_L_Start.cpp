#include "F_L_Start.h"

//构造函数
F_L_Start::F_L_Start()
{
	//---初始化变量---
	//加载界面图
	loadimage(&interfaceImg, "./frontend/assets/StartInterface.png");

	RECT r_draw, r_select;
	Ch* ch;

	//游客账号 输入文字 初始化
	ch = new(Ch);
	r_draw = { 1000, 600, 1000, 600 }, r_select = { 1000, 600, 1000, 600 };
	*ch = Ch("visitor", COLOR_NORMAL, r_select, r_select, 40);
	inputStr.push_back(ch);

	//账号登录 按钮文字 初始化
	ch = new(Ch);
	r_draw = { 505,452, 685,490 }, r_select = { 435,450, 685,490 };
	*ch = Ch(Login, "账号登录", COLOR_NORMAL, r_draw, r_select, 35);
	buttonStr.push_back(ch);
	//游客登录 按钮文字 初始化
	ch = new(Ch);
	r_draw = { 505, 521, 685, 560 }, r_select = { 435, 520, 685, 560 };
	*ch = Ch(Center, "游客登录", COLOR_NORMAL, r_draw, r_select, 35);
	buttonStr.push_back(ch);


	//---图标变量初始化---
	int x_draw, y_draw;
	Icon* icon;

	icon = new(Icon);
	x_draw = 1080, y_draw = 0, r_select = { 1080, 0, 1120, 40 };
	IMAGE* icon_normal = new(IMAGE);
	IMAGE* icon_normal_mask = new(IMAGE);
	IMAGE* icon_select = new(IMAGE);
	IMAGE* icon_select_mask = new(IMAGE);
	loadimage(icon_normal, "./frontend/assets/icon_quit.png");
	loadimage(icon_normal_mask, "./frontend/assets/icon_quit_mask.png");
	loadimage(icon_select, "./frontend/assets/icon_quit_select.png");
	loadimage(icon_select_mask, "./frontend/assets/icon_quit_select_mask.png");
	*icon = Icon(QuitSystem, icon_normal, icon_normal_mask, icon_select, icon_select_mask, x_draw, y_draw, r_select);
	quitButtonIcon.push_back(icon);
}

//析构函数
F_L_Start::~F_L_Start()
{
	//释放提示文字内存
	for (vector<Ch*>::iterator it = hintStr.begin(); it != hintStr.end(); it++)
	{
		delete (*it);
	}
	//释放输入文字内存
	for (vector<Ch*>::iterator it = inputStr.begin(); it != inputStr.end(); it++)
	{
		delete (*it);
	}
	//释放按钮文字内存
	for (vector<Ch*>::iterator it = buttonStr.begin(); it != buttonStr.end(); it++)
	{
		delete (*it);
	}
	//释放按钮图标内存
	for (vector<Icon*>::iterator it = quitButtonIcon.begin(); it != quitButtonIcon.end(); it++)
	{
		delete (*it)->icon_normal;
		delete (*it)->icon_normal_mask;
		delete (*it)->icon_select;
		delete (*it)->icon_select_mask;
		delete (*it);
	}
}



//绘制函数在父类中已实现

//更新函数已在父类中实现

//退出函数已在父类中实现