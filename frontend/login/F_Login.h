#pragma once

#include "../Front.h"
#include "../../config/help.h"
#include"../../backend/login/B_Login.h"


class F_Login : public Front{
public:

	//绘制函数
	void Draw(RECT notDrawRect = { 0 });//绘制函数
	void Update(ExMessage msg);//更新函数
	virtual ButtonDistinction Quit(ExMessage msg);//退出函数（可重写）

	//登录后端类对象
	B_Login b_login;

	//界面图
	IMAGE interfaceImg;

	//提示文字数组
	vector<Ch*>hintStr;

	//输入文字数组
	vector<Ch*>inputStr;

	//转跳界面按钮文字数组
	vector<Ch*>buttonStr;

	//转跳界面按钮图标数组
	vector<Icon*>quitButtonIcon;
};