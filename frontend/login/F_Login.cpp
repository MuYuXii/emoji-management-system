#include "F_Login.h"


//绘制函数
void F_Login::Draw(RECT notDrawRect)
{
	{

		//清屏
		cleardevice();
		//绘制界面图
		putimage(0, 0, &interfaceImg);
		//绘制 提示文字
		for (vector<Ch*>::iterator it = hintStr.begin(); it != hintStr.end(); it++)
		{
			(*it)->DrawChar();
		}
		//绘制 输入文字
		for (vector<Ch*>::iterator it = inputStr.begin(); it != inputStr.end(); it++)
		{
			if ((*it)->rect_draw.right != notDrawRect.right
				|| (*it)->rect_draw.left != notDrawRect.left
				|| (*it)->rect_draw.bottom != notDrawRect.bottom
				|| (*it)->rect_draw.top != notDrawRect.top)
			{
				(*it)->DrawChar();
			}
		}
		//绘制 转跳界面按钮文字
		for (vector<Ch*>::iterator it = buttonStr.begin(); it != buttonStr.end(); it++)
		{
			(*it)->DrawChar();
		}
		//绘制 转跳界面按钮图标
		for (vector<Icon*>::iterator it = quitButtonIcon.begin(); it != quitButtonIcon.end(); it++)
		{
			(*it)->DrawIcon();
		}

	}
}

//更新函数
void F_Login::Update(ExMessage msg)
{

	//根据鼠标位置更新提示文字颜色
	for (vector<Ch*>::iterator it = hintStr.begin(); it != hintStr.end(); it++)
	{
		(*it)->UpdateColor(msg);
	}
	//若点击输入文字选区，进入该输入文字的修改函数
	for (vector<Ch*>::iterator it = inputStr.begin(); it != inputStr.end(); it++)
	{
		if ((*it)->IsClick(msg))
		{
			(*it)->ChangeChar(this);
		}
	}
	//根据鼠标位置更新按钮文字颜色
	for (vector<Ch*>::iterator it = buttonStr.begin(); it != buttonStr.end(); it++)
	{
		(*it)->UpdateColor(msg);
	}
	//根据鼠标位置更新按钮图标状态
	for (vector<Icon*>::iterator it = quitButtonIcon.begin(); it != quitButtonIcon.end(); it++)
	{
		(*it)->UpdateStatus(msg);
	}
}

//退出函数
ButtonDistinction F_Login::Quit(ExMessage msg)
{
	//判断 转跳界面按钮文字 的退出转跳指令
	for (vector<Ch*>::iterator it = buttonStr.begin(); it != buttonStr.end(); it++)
	{
		if ((*it)->IsClick(msg))
		{
			//退出程序
			if ((*it)->distinction == QuitSystem)
				return QuitSystem;
			//转跳登录界面
			if ((*it)->distinction == Login)
				return Login;
			//转跳开始界面
			if ((*it)->distinction == Start)
				return Start;
			//转跳注册界面
			if ((*it)->distinction == Regist)
				return Regist;
			//转跳找回密码界面
			if ((*it)->distinction == Retrieve)
				return Retrieve;
			//转跳主界面
			if ((*it)->distinction == Center)
			{
				Account = inputStr[0]->str;
				return Center;
			}

		}
	}

	//判断 转跳界面按钮图标 的退出转跳指令
	for (vector<Icon*>::iterator it = quitButtonIcon.begin(); it != quitButtonIcon.end(); it++)
	{
		if ((*it)->IsClick(msg))
		{
			//退出程序
			if ((*it)->distinction == QuitSystem)
				return QuitSystem;
			//转跳登录界面
			if ((*it)->distinction == Login)
				return Login;
			//转跳开始界面
			if ((*it)->distinction == Start)
				return Start;
			//转跳注册界面
			if ((*it)->distinction == Regist)
				return Regist;
			//转跳找回密码界面
			if ((*it)->distinction == Retrieve)
				return Retrieve;
			//转跳主界面
			if ((*it)->distinction == Center)
			{
				Account = inputStr[0]->str;
				return Center;
			}
		}
	}
}