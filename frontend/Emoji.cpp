#include "Emoji.h"

Emoji::Emoji() {}

Emoji::Emoji(IMAGE* picture, char* name, IMAGE* favorite, IMAGE* notFavorite, IMAGE* del, int x_draw, int y_draw, RECT rect_select, int isLike) {
    this->picture = picture;

    this->name = new Ch;
    strcpy(this->name->str, name);
    this->name->color = BLACK;
    this->name->height = 20;
    this->name->width = 0;
    this->name->rect_select = rect_select;
    this->name->rect_draw = { rect_select.left + 70, rect_select.bottom, rect_select.right, rect_select.bottom + 20 };
    this->name->distinction = None;

    this->favorite = favorite;
    this->notFavorite = notFavorite;
    this->del = del;
    this->x_draw = x_draw;
    this->y_draw = y_draw;
    this->rect_select = rect_select;

    if (isLike == 1)
    {
        this->isLikeStatus = isFavorite;
    }
    else
    {
        this->isLikeStatus = isNotFavorite;
    }
};


void Emoji::DrawEmoji() {
    if (this->isChangeStatus == none)
    {
        putimage(x_draw, y_draw, picture);

    }
    else
    {
        putimage(x_draw, y_draw, picture);
        putimage(rect_select.left + 75, rect_select.top + 100, del);

        if (this->isLikeStatus == isFavorite)
            putimage(rect_select.left, rect_select.top + 100, favorite);
        else
            putimage(rect_select.left, rect_select.top + 100, notFavorite);

    }
    this->name->DrawChar();
}

void Emoji::UpdateEmjStatus(ExMessage msg) {
    if (msg.x >= rect_select.left && msg.x <= rect_select.right && msg.y <= rect_select.bottom && msg.y >= rect_select.top)
    {
        if (this->isChangeStatus != addMenu)
            this->isChangeStatus = addMenu;
    }
    else
    {
        if (this->isChangeStatus != none)
            this->isChangeStatus = none;
    }

    
    if ((this->isChangeStatus == addMenu) && LikeIsClick(msg) == true)
    {
        if (this->isLikeStatus == isFavorite)
            this->isLikeStatus = isNotFavorite;
        else
            this->isLikeStatus = isFavorite;
    }
}

bool Emoji::LikeIsClick(ExMessage msg)
{
    if (msg.x >= rect_select.left && msg.x <= rect_select.right - 75 && msg.y <= rect_select.bottom && msg.y >= rect_select.top + 100)
    {
        if (msg.message == WM_LBUTTONDOWN)
        {
            return true;
        }
    }
    return false;
}
    
bool Emoji::DeleteIsClick(ExMessage msg)
{
    if (isChangeStatus == addMenu)
    {
        if (msg.x >= rect_select.left + 75 && msg.x <= rect_select.right && msg.y <= rect_select.bottom && msg.y >= rect_select.top + 100)
        {
            if (msg.message == WM_LBUTTONDOWN)
            {
                return true;
            }
        }
    }
    return false;
}