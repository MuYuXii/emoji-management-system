#pragma once

#include <graphics.h>
#include <vector>
#include"Ch.h"
#include"Emoji.h"
#include"Icon.h"
using namespace std;
#define         WIDTH                 950         //easyX宽度
#define         HEIGHT                650         //easyX高度

//文字颜色
#define            COLOR_NORMAL        BLACK       //普通状态颜色
#define            COLOR_SELECT        RED         //选中状态颜色

// easyX封装
class Front{
public:

    virtual void Draw(RECT notDrawRect = { 0 }) = 0;//绘制函数  在绘制输入文字时，与notDrawRect相同绘制区域的文字不得绘制 
    virtual void Update(ExMessage msg) = 0;//更新函数
    virtual ButtonDistinction Quit(ExMessage msg) = 0;//退出函数

    string Account;

};